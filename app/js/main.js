$(window).bind("load", function() {
    $('#work-in-progress').fadeOut(100);
});
var profile;
var id_token;
$(document).ready(function() {
    $(".cross").hide();
    $(".menu").hide();
    $("#nav-login li").html('Log In');
    $(".hamburger").click(function() {
        $(".menu").slideToggle("slow", function() {
            $(".hamburger").hide();
            $(".cross").show();
        });
    });

    $(".cross").click(function() {
        $(".menu").slideToggle("slow", function() {
            $(".cross").hide();
            $(".hamburger").show();
        });
    });

    function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        // The ID token you need to pass to your backend:
        id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);
        openModal();
    };

    function signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function() {
            console.log('User signed out.');
            logoutModal();
        });
    }
});

function openModal() {
    $('#login-modal').modal();
}

function logoutModal() {
    $('#logout-modal').modal();
}

function changePage() {
    window.location("./login.html");
    $(".hamburger").hide();
}